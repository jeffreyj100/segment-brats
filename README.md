# BRATS
### Prerequisites:
- Installed required python dependencies via pip.
- Install Keras with Tensorflow backend.
- Change Keras to use Theano image dimension ordering:
    - vim /home/{username}/.keras
    - Replace with:
        ```
         {
            "image_dim_ordering": "th", 
            "epsilon": 1e-07, 
            "floatx": "float32", 
            "backend": "tensorflow"
        }
        ```




### Testing Network Models:
1. Download BRATS2015_Training data and place inside /code directory.
2. Run: python -W ignore data_preprocessing.py
3. Change configurations in nn_models.py as needed.
4. Run: python nn_models.py

