import numpy as np
import subprocess
import random
import progressbar
import fnmatch
import os
from glob import glob
from skimage import io, exposure

np.random.seed(1)

class Data_Preprocess(object):

    def __init__(self, path, n4itk_apply=False):
        self.path = path
        self.n4itk_apply = n4itk_apply
        self.modes = ['flair', 't1', 't1c', 't2', 'gt']
        self.slices_by_mode, by_slice = self.extract_data() # slices=[[flair], [t1], [t1c], [t2], [gt]], 155 per modality
        self.slices_by_slice = by_slice # [[slice1 x 5], [slice2 x 5], ..., [slice155 x 5]]
        self.normed_slices = self.norm_slices()

    def extract_data(self):
        '''
        for each patient directory go into each modality and load their scans.
        transforms each slice into vertical strip of 5 images
        '''
        print '\nLoading scans...'
        slices_by_mode = np.zeros((5, 155, 240, 240))
        slices_by_slice = np.zeros((155, 5, 240, 240))
        flair = glob(self.path + '/*Flair*/*.mha')
        t2 = glob(self.path + '/*_T2*/*.mha')
        gt = glob(self.path + '/*more*/*.mha')
        t1 = glob(self.path + '/**/*T1*.mha')
        if self.n4itk_apply:
            print 'Applying bias correction...'
            for t1_path in t1:
                if not fnmatch.fnmatch(t1_path, '*_n.mha'):
                    self.n4itk_norm(t1_path) # normalize files
        t1_n4 = glob(self.path + '/**/*T1*_n.mha')
        print 'flair[0] = ', flair[0]
        print 't1_n4[0] = ', t1_n4[0]
        print 't1_n4[1] = ', t1_n4[1]
        print 't2[0] = ', t2[0]
        print 'gt[0] = ', gt[0]
        scans = [flair[0], t1_n4[0], t1_n4[1], t2[0], gt[0]] # directories to each image (5 total)
        for scan_idx in xrange(5):
            # read each image directory, save to self.slices
            slices_by_mode[scan_idx] = io.imread(scans[scan_idx], plugin='simpleitk').astype(float)
        for mode_num in xrange(slices_by_mode.shape[0]): # modes 1 thru 5
            for slice_num in xrange(slices_by_mode.shape[1]): # slices 1 thru 155
                slices_by_slice[slice_num][mode_num] = slices_by_mode[mode_num][slice_num] # reshape by slice
        return slices_by_mode, slices_by_slice

    def norm_slices(self):
        '''
        normalizes each slice in self.slices_by_slice, excluding gt
        subtract mean and div by std dev for each slice
        clips top and bottom one percent of pixel intensities
        if n4itk == True, apply n4itk bias correction to T1 and T1c images
        '''
        print 'Normalizing slices...'
        normed_slices = np.zeros((155, 5, 240, 240))
        for slice_num in xrange(155):
            normed_slices[slice_num][-1] = self.slices_by_slice[slice_num][-1]
            for mode_num in xrange(4):
                normed_slices[slice_num][mode_num] = self._normalize(self.slices_by_slice[slice_num][mode_num])
        print 'Complete.'
        return normed_slices

    def _normalize(self, slice):
        '''
        INPUT:  (1) a single slice by any given modality (excluding gt)
                (2) index of modality assoc with slice (0=flair, 1=t1, 2=t1c, 3=t2)
        OUTPUT: normalized slice
        '''
        bottom, top = np.percentile(slice, (0.5, 99.5))
        slice = np.clip(slice, bottom, top)
        if np.std(slice) == 0:
            return slice
        else:
            return (slice - np.mean(slice)) / np.std(slice)

    def save_patient(self, patient_num):
        '''
        INPUT:  (1) string 'slice_type': unique identifier for each patient
                (2) int 'patient_num': unique identifier for each patient
        OUTPUT: saves png in Norm_PNG directory for normed, Training_PNG for reg
        '''
        print 'Saving scans for patient {} on path {}...'.format(patient_num, self.path)
        for slice_num in xrange(155): # reshape to strip
            strip = self.normed_slices[slice_num].reshape(1200, 240)
            if np.max(strip) != 0: # set values < 1
                strip /= np.max(strip)
            if np.min(strip) <= -1: # set values > -1
                strip /= abs(np.min(strip))
            io.imsave('N4_PNG/{}_{}.png'.format(patient_num, slice_num), strip)

    def n4itk_norm(self, input_img, n_dims=3, n_iters='[20,20,10,5]'):
        '''
        INPUT:  (1) filepath 'input_img': path to mha T1 or T1c file
                (2) directory 'parent_dir': parent directory to mha file
        OUTPUT: writes n4itk normalized image to parent_dir under orig_filename_n.mha
        '''
        print 'Applying N4 correction on path = ', input_img
        output_img = input_img[:-4] + '_n.mha'
        # python n4_bias_correction.py [inputImage (ex. image001.mha)] [outputImage (ex. image001_corrected.mha)]
        subprocess.call('python n4_bias_correction.py ' + input_img + ' ' + output_img,
                        shell = True)
        print 'Completed N4 correction.'

def save_patient_slices(patients):
    '''
    INPUT   (1) list 'patients': paths to any directories of patients to save. for example - glob("Training/HGG/**")
            (2) string 'type': options = reg (non-normalized), norm (normalized, but no bias correction), 
                    n4 (bias corrected and normalized) saves strips of patient slices to appropriate directory
                    (Training_PNG/, Normalized_PNG/, N4_PNG/) as patient-num_slice-num
    '''
    for patient_num, path in enumerate(patients):
        a = Data_Preprocess(path)
        a.save_patient(patient_num)

def save_labels(labels):
    '''
    INPUT list 'labels': filepath to all labels
    '''
    for label_num in xrange(len(labels)):
        print 'Saving label {}'.format(label_num)
        slices = io.imread(labels[label_num], plugin = "simpleitk")
        for slice_num in xrange(len(slices)):
            io.imsave('Labels/{}_{}L.png'.format(label_num, slice_num), slices[slice_num])

if __name__ == '__main__':
    #os.getcwd()
    #os.mkdir('Labels')
    #os.mkdir('N4_PNG')

    labels = glob('BRATS2015_Training/HGG/**/*more*/**.mha')
    save_labels(labels)
    
    #patients = glob('BRATS2015_Training/HGG/**')
    #save_patient_slices(patients)

        
