from keras.layers import Input, merge, Dropout, Dense, Lambda, Flatten, Activation
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import MaxPooling2D, Convolution2D, AveragePooling2D
from keras.models import Model
from keras.optimizers import SGD, Nadam
from keras import backend as K

from metrics import dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int
from multi_gpu import make_parallel

def stem(inputs):
    '''
    INPUT (1)
    '''
    x = Convolution2D(8, 3, 3, activation='relu', subsample=(2,2))(inputs)
    x = Convolution2D(8, 3, 3, activation='relu',)(x)
    x = Convolution2D(16, 3, 3, activation='relu',)(x)

    x = MaxPooling2D((3,3), strides=(1,1))(x)

    x = Convolution2D(20, 1, 1, activation='relu', border_mode='same')(x)
    x = Convolution2D(48, 3, 3, activation='relu')(x)
    x = Convolution2D(64, 3, 3, activation='relu', subsample=(3,3), border_mode='same')(x)

    x = BatchNormalization(axis=1)(x)
    x = Activation('relu')(x)
    return x

def module_a(inputs):
    '''
    INPUT (1)
    '''
    orig_inputs = inputs

    a1 = Convolution2D(8, 1, 1, activation='relu', border_mode='same')(inputs)

    a2 = Convolution2D(8, 1, 1, activation='relu', border_mode='same')(inputs)
    a2 = Convolution2D(8, 4, 4, activation='relu', border_mode='same')(a2)

    a3 = Convolution2D(8, 1, 1, activation='relu', border_mode='same')(inputs)
    a3 = Convolution2D(8, 4, 4, activation='relu', border_mode='same')(a3)
    a3 = Convolution2D(8, 4, 4, activation='relu', border_mode='same')(a3)

    a_merge = merge([a1, a2, a3], concat_axis=1, mode='concat')

    conv = Convolution2D(64, 1, 1, activation='linear', border_mode='same')(a_merge)
    conv = Lambda(lambda x: x * 0.1)(conv)

    x = merge([orig_inputs, conv], mode='sum')
    x = BatchNormalization(axis=1)(x)
    x =  Activation('relu')(x)
    return x

def module_b(inputs):
    '''
    INPUT (1)
    '''
    orig_inputs = inputs

    a1 = Convolution2D(32, 1, 1, activation='relu', border_mode='same')(inputs)

    a2 = Convolution2D(32, 1, 1, activation='relu', border_mode='same')(inputs)
    a2 = Convolution2D(32, 1, 7, activation='relu', border_mode='same')(a2)
    a2 = Convolution2D(32, 7, 1, activation='relu', border_mode='same')(a2)

    b_merge = merge([a1, a2], mode='concat', concat_axis=1)

    conv = Convolution2D(224, 1, 1, activation='linear', border_mode='same')(b_merge)
    conv = Lambda(lambda x: x * 0.1)(conv)

    x = merge([orig_inputs, conv], mode='sum')
    x = BatchNormalization(axis=1)(x)
    x = Activation('relu')(x)
    return x

def module_c(inputs):
    '''
    INPUT (1)
    '''
    orig_inputs = inputs

    a1 = Convolution2D(32, 1, 1, activation='relu', border_mode='same')(inputs)

    a2 = Convolution2D(48, 1, 1, activation='relu', border_mode='same')(inputs)
    a2 = Convolution2D(48, 1, 3, activation='relu', border_mode='same')(a2)
    a2 = Convolution2D(48, 3, 1, activation='relu', border_mode='same')(a2)

    c_merge = merge([a1, a2], mode='concat', concat_axis=1)

    conv = Convolution2D(448, 1, 1, activation='linear', border_mode='same')(c_merge)
    conv = Lambda(lambda x: x * 0.1)(conv)

    x = merge([orig_inputs, conv], mode='sum')
    x = BatchNormalization(axis=1)(x)
    x = Activation('relu')(x)
    return x
    
def dim_reduction_a(inputs):
    '''
    INPUT (1)
    '''
    r1 = MaxPooling2D((2,2), strides=(1,1))(inputs)

    r2 = Convolution2D(96, 2, 2, activation='relu', subsample=(1,1))(inputs)

    r3 = Convolution2D(48, 1, 1, activation='relu', border_mode='same')(inputs)
    r3 = Convolution2D(56, 2, 2, activation='relu', border_mode='same')(r3)
    r3 = Convolution2D(64, 2, 2, activation='relu', subsample=(1,1))(r3)

    red_merge = merge([r1, r2, r3], mode='concat', concat_axis=1)
    red_merge = BatchNormalization(axis=1)(red_merge)
    red_merge = Activation('relu')(red_merge)
    return red_merge

def dim_reduction_b(inputs):
    '''
    INPUT (1)
    '''
    r1 = MaxPooling2D((2,2), strides=(1,1), border_mode='valid')(inputs)

    r2 = Convolution2D(64, 1, 1, activation='relu', border_mode='same')(inputs)
    r2 = Convolution2D(96, 2, 2, activation='relu', subsample=(1,1))(r2)
    
    r3 = Convolution2D(64, 1, 1, activation='relu', border_mode='same')(inputs)
    r3 = Convolution2D(64, 2, 2, activation='relu', subsample=(1,1))(r3)

    r4 = Convolution2D(64, 1, 1, activation='relu', border_mode='same')(inputs)
    r4 = Convolution2D(64, 2, 2, activation='relu', border_mode='same')(r4)
    r4 = Convolution2D(64, 2, 2, activation='relu', subsample=(1,1))(r4)

    red_merge = merge([r1, r2, r3, r4], concat_axis=1, mode='concat')
    red_merge = BatchNormalization(axis=1)(red_merge)
    red_merge = Activation('relu')(red_merge)
    return red_merge

def comp_inception_resnetv1(multi_gpu, num_gpu):
    '''
    compiles inception resnetv1
    '''
    print 'Compiling inception resnetv1 ...'
    orig_inputs = Input(shape=(4, 33, 33))

    x = stem(orig_inputs)

    for i in range(5):
        x = module_a(x)

    x = dim_reduction_a(x)

    for i in range(10):
        x = module_b(x)

    x = dim_reduction_b(x)

    for i in range(5):
        x = module_c(x)

    x = AveragePooling2D((1,1))(x)
    x = Dropout(0.8)(x)

    x = Flatten()(x)
    x = Dense(256)(x)
    x = Dense(5, activation='softmax')(x)

    inception_resnetv1 = Model(orig_inputs, x)

    if multi_gpu:
        inception_resnetv1 = make_parallel(inception_resnetv1, num_gpu)

    nadam = Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004)
    inception_resnetv1.compile(optimizer='nadam', loss='categorical_crossentropy', metrics=[dice_coef, 
        dice_coef_neg, jaccard_similarity, jaccard_similarity_int])
    print 'Complete.'
    return inception_resnetv1