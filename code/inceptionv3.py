import numpy as np
import sys
import warnings
from keras.models import Model
from keras.layers import Input, Dense, Flatten, merge, Lambda, Activation
from keras.layers.convolutional import Convolution2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D
from keras.layers.normalization import BatchNormalization
from keras.regularizers import l2
from keras.optimizers import RMSprop
import keras.backend as K

from metrics import dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int
from multi_gpu import make_parallel

def bn_conv(num_filter, num_row, num_col, inputs, subsample=(1,1), border_mode='same'):
    '''
    INPUT (1) num_filter: number of filters
          (2) num_row: filter height
          (3) num_col: filter width
          (4) inputs: input tensor
          (4) subsample: same as strides, default - (1,1)
          (5) border_mode: default - 'same'
    OUTPUT (1) batch normalized tensor
    '''
    conv = Convolution2D(num_filter, num_row, num_col, subsample=subsample, border_mode=border_mode)(inputs)
    act = Activation('relu')(conv)
    return BatchNormalization(mode=0, axis=1)(act)

def module_5(inputs):
    '''
    INPUT (1) inputs: input tensor
    '''
    # Tower A
    conv_a1 = bn_conv(16, 1, 1, inputs=inputs)
    conv_a2 = bn_conv(24, 3, 3, inputs=conv_a1)
    conv_a3 = bn_conv(24, 3, 3, inputs=conv_a2)

    # Tower B
    conv_b1 = bn_conv(12, 1, 1, inputs=inputs)
    conv_b2 = bn_conv(16, 3, 3, inputs=conv_b1)

    # Tower C
    pool_c1 = AveragePooling2D(pool_size=(3,3), strides=(1,1), border_mode='same')(inputs)
    conv_c2 = bn_conv(16, 1, 1, inputs=pool_c1)

    # Tower D
    conv_d1 = bn_conv(16, 1, 1, inputs=inputs)

    return merge([conv_a3, conv_b2, conv_c2, conv_d1], mode='concat', concat_axis=1)

def module_6(inputs):
    '''
    INPUT (1) inputs: input tensor
    '''
    # Tower A
    conv_a1 = bn_conv(32, 1, 1, inputs=inputs)
    conv_a2 = bn_conv(32, 1, 7, inputs=conv_a1)
    conv_a3 = bn_conv(32, 7, 1, inputs=conv_a2)
    conv_a4 = bn_conv(32, 1, 7, inputs=conv_a3)
    conv_a5 = bn_conv(48, 7, 1, inputs=conv_a4)

    # Tower B
    conv_b1 = bn_conv(32, 1, 1, inputs=inputs)
    conv_b2 = bn_conv(32, 1, 7, inputs=conv_b1)
    conv_b3 = bn_conv(48, 7, 1, inputs=conv_b2)

    # Tower C
    pool_c1 = AveragePooling2D(pool_size=(3,3), strides=(1,1), border_mode='same')(inputs)
    conv_c2 = bn_conv(48, 1, 1, inputs=pool_c1)

    # Tower D
    conv_d = bn_conv(48, 1, 1, inputs=inputs)

    return merge([conv_a5, conv_b3, conv_c2, conv_d], mode='concat', concat_axis=1)

def module_7(inputs):
    '''
    INPUT (1) inputs: input tensor
    '''
    # Tower A
    conv_a1 = bn_conv(112, 1, 1, inputs=inputs)
    conv_a2 = bn_conv(96, 3, 3, inputs=conv_a1)
    conv_a3 = bn_conv(96, 1, 3, inputs=conv_a2)
    conv_a4 = bn_conv(96, 3, 1, inputs=conv_a3)

    # Tower B
    conv_b1 = bn_conv(96, 1, 1, inputs=inputs)
    conv_b2 = bn_conv(96, 1, 3, inputs=conv_b1)
    conv_b3 = bn_conv(96, 3, 1, inputs=conv_b2)

    # Tower C
    pool_c1 = AveragePooling2D(pool_size=(3,3), strides=(1,1), border_mode='same')(inputs)
    conv_c2 = bn_conv(48, 1, 1, inputs=pool_c1)

    # Tower D
    conv_d = bn_conv(80, 1, 1, inputs=inputs)

    return merge([conv_a4, conv_b3, conv_c2, conv_d], mode='concat', concat_axis=1)

def dim_reduction_a(inputs):
    '''
    INPUT (1) inputs: input tensor
    '''
    # Tower A
    conv_a1 = bn_conv(16, 3, 3, inputs=inputs)
    conv_a2 = bn_conv(24, 1, 1, inputs=conv_a1)
    conv_a3 = bn_conv(24, 1, 1, inputs=conv_a2, subsample=(2,2), border_mode='valid')

    # Tower B
    conv_b = bn_conv(96, 1, 1, inputs=inputs, subsample=(2,2), border_mode='valid')

    # Tower C
    pool_c = MaxPooling2D(pool_size=(1,1), strides=(2,2), border_mode='valid')(inputs)

    return merge([conv_a3, conv_b, pool_c], mode='concat', concat_axis=1)

def dim_reduction_b(inputs):
    '''
    INPUT (1) inputs: input tensor
    '''
    # Tower A
    conv_a1 = bn_conv(48, 3, 3, inputs=inputs)
    conv_a2 = bn_conv(80, 1, 1, inputs=conv_a1, subsample=(2,2), border_mode='valid')

    # Tower B
    conv_b1 = bn_conv(48, 1, 1, inputs=inputs)
    conv_b2 = bn_conv(48, 1, 7, inputs=conv_b1)
    conv_b3 = bn_conv(48, 7, 1, inputs=conv_b2)
    conv_b4 = bn_conv(48, 1, 1, inputs=conv_b3, subsample=(2,2), border_mode='valid')

    # Tower C
    pool_c = MaxPooling2D(pool_size=(1,1), strides=(2,2), border_mode='valid')(inputs)

    return merge([conv_a2, conv_b4, pool_c], mode='concat', concat_axis=1)

def comp_inceptionv3(multi_gpu, num_gpu):
    '''
    compiles inceptionv3
    '''
    print 'Compiling inceptionv3 ...'
    inputs = Input(shape=(4, 33, 33))

    conv_1 = bn_conv(8, 3, 3, inputs=inputs, subsample=(2,2), border_mode='valid')
    conv_2 = bn_conv(8, 3, 3, inputs=conv_1, border_mode='valid')
    conv_3 = bn_conv(16, 3, 3, inputs=conv_2)
    pool_4 = MaxPooling2D(pool_size=(3,3), strides=(2,2), border_mode='valid')(conv_3)

    conv_5 = bn_conv(20, 1, 1, inputs=pool_4)
    conv_6 = bn_conv(48, 3, 3, inputs=conv_5, border_mode='valid')
    pool_7 = MaxPooling2D(pool_size=(3,3), strides=(2,2), border_mode='valid')(conv_6)

    incep_8 = module_5(pool_7)
    incep_9 = module_5(incep_8)
    incep_10 = module_5(incep_9)

    incep_11 = dim_reduction_a(incep_10)
    
    incep_12 = module_6(incep_11)
    incep_13 = module_6(incep_12)
    incep_14 = module_6(incep_13)
    incep_15 = module_6(incep_14)
    incep_16 = module_6(incep_15)

    incep_17 = dim_reduction_b(incep_16)

    incep_18 = module_7(incep_17)
    incep_19 = module_7(incep_18)

    x = AveragePooling2D((1, 1), name='avg_pool')(incep_19)

    x = Flatten()(x)
    x = Dense(256)(x)
    x = Dense(5, activation='softmax')(x)

    inceptionv3 = Model(inputs, x)

    if multi_gpu:
        inceptionv3 = make_parallel(inceptionv3, num_gpu)

    rmsprop = RMSprop(lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0)
    inceptionv3.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=[dice_coef, 
        dice_coef_neg, jaccard_similarity, jaccard_similarity_int])
    print 'Complete.'
    return inceptionv3
