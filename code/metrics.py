from keras.models import Sequential, model_from_json
from keras.layers import Input, Dense, merge, advanced_activations
from keras.models import Model, load_model
from keras.layers.convolutional import Convolution2D, MaxPooling2D, UpSampling2D
from keras.layers.core import Flatten
from keras.layers.normalization import BatchNormalization
from keras.optimizers import SGD
from keras.constraints import maxnorm
from keras import backend as K

import tensorflow as tf
import numpy as np
from numpy.core.umath_tests import inner1d
from sklearn.metrics import classification_report, jaccard_similarity_score

def dice_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    similar = K.sum(y_true_f * y_pred_f)
    return (similar * 2.0) / (K.sum(y_true_f) + K.sum(y_pred_f))

def dice_coef_neg(y_true, y_pred):
    return -dice_coef(y_true, y_pred)

def hausdorff_dist(y_true, y_pred):
    def euclid_dist(val1, val2):
        dist = 0.0
        for i in range(len(val1.get_shape())):
            d = val1[i] - val2[i]
            dist += d * d
        return dist

    def max_dist_minima(val1, val2):
        minima = [] 
        minima.append(len(val1.get_shape()))
        sum_w = 0
        for i in range(len(val1.get_shape())):
            for j in range(len(val2.get_shape())):
                dist = euclid_dist(val1[i], val2[j])
                print 'dist = ', dist
                print 'minima[i] = ', minima[i]
                
                if(dist < minima[i]):
                    minima[i] = dist
        max_val = 0.0
        for i in range(len(minima.get_shape())):
            if(minima[i] > max_val):
                max_val = minima[i]
        return max_val

    max1 = max_dist_minima(y_true, y_pred)
    max2 = max_dist_minima(y_pred, y_true)
    return K.max(max1, max2)

def jaccard_similarity(y_true, y_pred):
    intersect = K.sum(y_true * y_pred, axis=[0, -1, -2])
    sum_val = K.sum(y_true + y_pred, axis=[0, -1, -2])
    jaccard = (intersect + 1e-12) / (sum_val - intersect + 1e-12)
    return K.mean(jaccard)

def jaccard_similarity_int(y_true, y_pred):
    y_pred_p = K.round(K.clip(y_pred, 0, 1))
    intersect = K.sum(y_true * y_pred_p, axis=[0, -1, -2])
    sum_val = K.sum(y_true + y_pred, axis=[0, -1, -2])
    jaccard = (intersect + 1e-12) / (sum_val - intersect + 1e-12)
    return K.mean(jaccard)