from keras.models import Sequential, model_from_json
from keras.layers import Input, Dense, merge, advanced_activations
from keras.models import Model, load_model
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.core import Dense, Dropout, Activation, Flatten, Merge, Reshape, MaxoutDense
from keras.layers.normalization import BatchNormalization
from keras.regularizers import l1l2
from keras.optimizers import SGD
from keras.constraints import maxnorm
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.utils import np_utils

from multi_gpu import make_parallel

def comp_dual(multi_gpu, num_gpu):
    '''
    dual model. Similar to 2 pathway, but takes in 4x33x33 patch and its center 4x3x3 patch. merges paths at 
    flatten layer
    '''
    print 'Compiling dual model...'
    single = Sequential()

    single.add(Convolution2D(64, 7, 7, border_mode='valid', W_regularizer=l1l2(l1=0.01, l2=0.01), 
                input_shape=(4,33,33)))
    single.add(Activation('relu'))
    single.add(BatchNormalization(mode=0, axis=1))
    single.add(MaxPooling2D(pool_size=(2,2), strides=(1,1)))
    single.add(Dropout(0.5))

    single.add(Convolution2D(nb_filter=128, nb_row=5, nb_col=5, activation='relu', border_mode='valid',
                W_regularizer=l1l2(l1=0.01, l2=0.01)))
    single.add(BatchNormalization(mode=0, axis=1))
    single.add(MaxPooling2D(pool_size=(2,2), strides=(1,1)))
    single.add(Dropout(0.5))

    single.add(Convolution2D(nb_filter=256, nb_row=5, nb_col=5, activation='relu', border_mode='valid',
                W_regularizer=l1l2(l1=0.01, l2=0.01)))
    single.add(BatchNormalization(mode=0, axis=1))
    single.add(MaxPooling2D(pool_size=(2,2), strides=(1,1)))
    single.add(Dropout(0.5))

    single.add(Convolution2D(nb_filter=128, nb_row=3, nb_col=3, activation='relu', border_mode='valid',
                W_regularizer=l1l2(l1=0.01, l2=0.01)))
    single.add(Dropout(0.25))
    single.add(Flatten())

    # add small patch to train on
    small = Sequential()
    small.add(Reshape((36,1), input_shape=(4,3,3)))
    small.add(Flatten())
    small.add(MaxoutDense(128, nb_feature=5))
    small.add(Dropout(0.5))

    model = Sequential()
    # merge both paths
    model.add(Merge([small, single], mode='concat', concat_axis=1))
    model.add(Dense(5))
    model.add(Activation('softmax'))

    if multi_gpu:
        model = make_parallel(model, num_gpu)

    sgd = SGD(lr=0.001, decay=0.01, momentum=0.9)
    model.compile(loss='categorical_crossentropy', optimizer='sgd')
    print 'Complete.'
    return model

def comp_two_path(multi_gpu, num_gpu):
    '''
    compiles two-path model, takes in a 4x33x33 patch and assesss global and local paths, then merges the results.
    '''
    print 'Compiling two-path model...'
    local_path = Sequential()

    local_path.add(Convolution2D(64, 7, 7, border_mode='valid', W_regularizer=l1l2(l1=0.01, l2=0.01), input_shape=                          (4,33,33)))
    local_path.add(Activation('relu'))
    local_path.add(MaxPooling2D(pool_size=(4,4), strides=(1,1)))
    local_path.add(Dropout(0.5))
    
    local_path.add(Convolution2D(nb_filter=64, nb_row=3, nb_col=3, activation='relu', border_mode='valid',
                    W_regularizer=l1l2(l1=0.01, l2=0.01)))
    local_path.add(MaxPooling2D(pool_size=(2,2), strides=(1,1)))
    local_path.add(Dropout(0.5))

    global_path = Sequential()

    global_path.add(Convolution2D(160, 13, 13, border_mode='valid', W_regularizer=l1l2(l1=0.01, l2=0.01), 
                    input_shape=(4,33,33)))
    global_path.add(Activation('relu'))
    global_path.add(Dropout(0.5))

    model = Sequential()

    model.add(Merge([local_path, global_path], mode='concat', concat_axis=1))
    model.add(Flatten())
    model.add(Dense(5))
    model.add(Activation('softmax'))

    if multi_gpu:
        model = make_parallel(model, num_gpu)

    sgd = SGD(lr=0.001, decay=0.01, momentum=0.9)
    model.compile(loss='categorical_crossentropy', optimizer='sgd')
    print 'Complete.'
    return model