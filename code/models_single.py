from keras.models import Sequential
from keras.layers import Input, Dense, merge, advanced_activations
from keras.models import Model, load_model
from keras.layers.convolutional import Convolution2D, MaxPooling2D, UpSampling2D
from keras.layers.core import Dense, Dropout, Activation, Flatten, Reshape, Permute
from keras.layers.normalization import BatchNormalization
from keras.regularizers import l1l2
from keras.optimizers import SGD, Nadam
from keras.constraints import maxnorm

import tensorflow as tf
import numpy as np
from numpy.core.umath_tests import inner1d
from sklearn.metrics import classification_report, jaccard_similarity_score

from metrics import dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int
from multi_gpu import make_parallel

def comp_singlev1(multi_gpu, num_gpu):
    '''
    compiles singlev1 model with 4 conv/max-pool layers
    results = 
    '''
    print 'Compiling singlev1 model...'
    singlev1 = Sequential()

    singlev1.add(Convolution2D(64, 7, 7, border_mode='valid', 
                W_regularizer=l1l2(l1=0.01, l2=0.01), input_shape=(4,33,33)))
    singlev1.add(Activation('relu'))
    singlev1.add(BatchNormalization(mode=0, axis=1))
    singlev1.add(MaxPooling2D(pool_size=(2,2), strides=(1,1)))
    singlev1.add(Dropout(0.5))

    singlev1.add(Convolution2D(128, 5, 5, activation='relu',
                border_mode='valid', W_regularizer=l1l2(l1=0.01, l2=0.01)))
    singlev1.add(Dropout(0.25))

    singlev1.add(Flatten())
    singlev1.add(Dense(5))
    singlev1.add(Activation('softmax'))

    if multi_gpu:
        singlev1 = make_parallel(singlev1, num_gpu)

    sgd = SGD(lr=0.001, decay=0.01, momentum=0.9)
    singlev1.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=[dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int])
    print 'Complete.'
    return singlev1

def comp_singlev2(multi_gpu, num_gpu):
    '''
    compiles single model with 4 conv/max-pool layers
    result = excellent
    '''
    print 'Compiling singlev2 model...'
    singlev2 = Sequential()

    singlev2.add(Convolution2D(32, 3, 3, border_mode='valid', W_regularizer=l1l2(l1=0.01, l2=0.01),
                input_shape=(4,33,33)))
    singlev2.add(Activation('relu'))
    
    singlev2.add(Convolution2D(32, 3, 3, border_mode='valid', activation='relu', 
                W_regularizer=l1l2(l1=0.01, l2=0.01)))
    singlev2.add(Dropout(0.1))

    singlev2.add(Convolution2D(64, 3, 3, border_mode='valid', activation='relu',
                W_regularizer=l1l2(l1=0.01, l2=0.01)))
    
    singlev2.add(Convolution2D(64, 3, 3, border_mode='valid', activation='relu'))
    singlev2.add(Dropout(0.1))

    singlev2.add(Flatten())
    singlev2.add(Dense(256))
    singlev2.add(Dense(5))
    singlev2.add(Activation('softmax'))

    if multi_gpu:
        singlev2 = make_parallel(singlev2, num_gpu)

    sgd = SGD(lr=0.001, decay=0.01, momentum=0.9)
    singlev2.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=[dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int])
    print 'Complete.'
    return singlev2

def comp_singlev3(multi_gpu, num_gpu):
    '''
    results = 
    '''
    print 'Compiling singlev3 model...'
    singlev3 = Sequential()

    # encoding layers
    singlev3.add(Convolution2D(64, 3, 3, border_mode='same', input_shape=(4, 33, 33)))
    singlev3.add(BatchNormalization())
    singlev3.add(Activation('relu'))
    #singlev3.add(Dropout(0.5))
    singlev3.add(Convolution2D(64, 3, 3, border_mode='same'))
    singlev3.add(BatchNormalization())
    singlev3.add(Activation('relu'))
    singlev3.add(MaxPooling2D())
    #singlev3.add(Dropout(0.5))

    singlev3.add(Convolution2D(96, 3, 3, border_mode='same'))
    singlev3.add(BatchNormalization())
    singlev3.add(Activation('relu'))
    #singlev3.add(Dropout(0.5))
    singlev3.add(Convolution2D(96, 3, 3, border_mode='same'))
    singlev3.add(BatchNormalization())
    singlev3.add(Activation('relu'))
    singlev3.add(MaxPooling2D())
    #singlev3.add(Dropout(0.5))

    singlev3.add(Convolution2D(128, 3, 3, border_mode='same'))
    singlev3.add(BatchNormalization())
    singlev3.add(Activation('relu'))
    #singlev3.add(Dropout(0.5))
    singlev3.add(Convolution2D(128, 3, 3, border_mode='same'))
    singlev3.add(BatchNormalization())
    singlev3.add(Activation('relu'))
    #singlev3.add(Dropout(0.5))
    singlev3.add(Convolution2D(128, 3, 3, border_mode='same'))
    singlev3.add(BatchNormalization())
    singlev3.add(Activation('relu'))
    singlev3.add(MaxPooling2D())
    #singlev3.add(Dropout(0.5))

    singlev3.add(Flatten())
    singlev3.add(Dense(5))
    singlev3.add(Activation('softmax'))

    if multi_gpu:
        singlev3 = make_parallel(singlev3, 2)

    nadam = Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004)
    singlev3.compile(optimizer='nadam', loss='categorical_crossentropy', metrics=[dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int])
    print 'Complete.'
    return singlev3

def comp_vgg19(multi_gpu, num_gpu):
    '''
    
    '''
    print 'Compiling vgg19 model...'
    vgg19 = Sequential()

    # encoding layers
    vgg19.add(Convolution2D(64, 3, 3, border_mode='same', input_shape=(4, 33, 33)))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(64, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    vgg19.add(MaxPooling2D())
    #vgg19.add(Dropout(0.5))

    vgg19.add(Convolution2D(96, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(96, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    vgg19.add(MaxPooling2D())
    #vgg19.add(Dropout(0.5))

    vgg19.add(Convolution2D(128, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(128, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(128, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(128, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    vgg19.add(MaxPooling2D())
    #vgg19.add(Dropout(0.5))

    vgg19.add(Convolution2D(160, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(160, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(160, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(160, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    vgg19.add(MaxPooling2D())
    #vgg19.add(Dropout(0.5))

    vgg19.add(Convolution2D(192, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(192, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(192, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    #vgg19.add(Dropout(0.5))
    vgg19.add(Convolution2D(192, 3, 3, border_mode='same'))
    vgg19.add(BatchNormalization())
    vgg19.add(Activation('relu'))
    vgg19.add(MaxPooling2D())

    vgg19.add(Flatten())
    vgg19.add(Dense(256))
    vgg19.add(Dense(5))
    vgg19.add(Activation('softmax'))

    if multi_gpu:
        vgg19 = make_parallel(vgg19, num_gpu)

    nadam = Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004)
    vgg19.compile(optimizer='nadam', loss='categorical_crossentropy', metrics=[dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int])
    print 'Complete.'
    return vgg19

def comp_small_alex(multi_gpu, num_gpu):
    '''
    result = poor
    '''
    print 'Compiling small alex model...'
    small_alex = Sequential()

    small_alex.add(Convolution2D(64, 11, 11, border_mode='valid', W_regularizer=l1l2(l1=0.01, l2=0.01),
                    input_shape=(4, 33, 33)))
    small_alex.add(Activation('relu'))
    small_alex.add(MaxPooling2D(pool_size=(2, 2), strides=(1, 1)))

    small_alex.add(Convolution2D(64, 5, 5, border_mode='valid', activation='relu',
                    W_regularizer=l1l2(l1=0.01, l2=0.01)))
    small_alex.add(MaxPooling2D(pool_size=(2, 2), strides=(1, 1)))

    small_alex.add(Convolution2D(64, 3, 3, border_mode='valid', activation='relu',
                    W_regularizer=l1l2(l1=0.01, l2=0.01)))

    small_alex.add(Convolution2D(64, 3, 3, border_mode='valid', activation='relu',
                    W_regularizer=l1l2(l1=0.01, l2=0.01)))

    small_alex.add(Convolution2D(64, 3, 3, border_mode='valid', activation='relu',
                    W_regularizer=l1l2(l1=0.01, l2=0.01)))

    small_alex.add(Flatten())
    small_alex.add(Dense(128))
    small_alex.add(Dropout(0.5))
    small_alex.add(Dense(128))
    small_alex.add(Dropout(0.5))
    small_alex.add(Dense(134))
    small_alex.add(Dropout(0.5))
    small_alex.add(Dense(5))
    small_alex.add(Activation('softmax'))

    if multi_gpu:
        small_alex = make_parallel(small_alex, num_gpu)

    sgd = SGD(lr=0.001, decay=0.01, momentum=0.9)
    small_alex.compile(loss='categorical_crossentropy', optimizer='sgd', metric=[dice_coef, jaccard_similarity, jaccard_similarity_int])
    print 'Complete.'
    return small_alex

def comp_segnet(multi_gpu, num_gpu):
    '''
    result = 
    '''
    print 'Compiling segnet model...'
    segnet = Sequential()

    # encoding layers
    segnet.add(Convolution2D(64, 3, 3, border_mode='same', input_shape=(4, 33, 33)))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(64, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(MaxPooling2D())

    segnet.add(Convolution2D(96, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(96, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(MaxPooling2D())

    segnet.add(Convolution2D(128, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(128, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(128, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(MaxPooling2D())

    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(MaxPooling2D())

    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(MaxPooling2D())

    # decoding layers
    segnet.add(UpSampling2D())
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))

    segnet.add(UpSampling2D())
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(160, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(128, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))

    segnet.add(UpSampling2D())
    segnet.add(Convolution2D(128, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(128, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(96, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))

    segnet.add(UpSampling2D())
    segnet.add(Convolution2D(96, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(64, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))

    segnet.add(UpSampling2D())
    segnet.add(Convolution2D(64, 3, 3, border_mode='same'))
    segnet.add(BatchNormalization())
    segnet.add(Activation('relu'))
    segnet.add(Convolution2D(4, 1, 1, border_mode='valid'))
    segnet.add(BatchNormalization())

    segnet.add(Flatten())
    segnet.add(Dense(5))
    segnet.add(Activation('softmax'))

    if multi_gpu:
        segnet = make_parallel(segnet, num_gpu)

    sgd = SGD(lr=0.001, decay=0.01, momentum=0.9)
    segnet.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=[dice_coef])
    print 'Complete.'
    return segnet

 