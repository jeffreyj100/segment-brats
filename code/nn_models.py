import models_single 
import models_double 
import resnet 
import inceptionv3
import inception_resnetv1

from patch_lib import PatchLib
from metrics import dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int

import numpy as np
import pandas as pd
import random, json, h5py, os, shutil, sys
from glob import glob
import matplotlib.pyplot as plt
from medpy import metric
from scipy import sparse
from skimage import io, color, img_as_float
from skimage.exposure import adjust_gamma
from skimage.segmentation import mark_boundaries
from sklearn.feature_extraction.image import extract_patches_2d
from sklearn.metrics import classification_report, jaccard_similarity_score, confusion_matrix
from keras.utils import np_utils
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.models import Model, load_model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils.np_utils import probas_to_classes

class NN_Models(object):
    def __init__(self, n_epoch=10, n_chan=4, batch_size=128, loaded_model=False, custom_metric=False, architecture='single', augment_data=False, gen_multiplier=2, multi_gpu=False, num_gpu=1, w_reg=0.01, n_filters=[64,128,128,128], k_dims=[7,5,5,3], activation='relu'):
        '''
        A class for compiling/loading, fitting, saving models, viewing segmented images and analyzing results
        INPUT:  (1) int 'n_epoch': number of epochs to train on. default = 10
                (2) int 'n_chan': number of channels being assessed. default = 4
                (3) int 'batch_size': number of images to train on for each batch. default = 128
                (4) bool 'loaded_model': True if loading pre-existing model. default = False
                (5) str 'architecture': type of model to use, options = single, dual, two_path. default = 'single'
                (6) float 'w_reg': value for l1 and l2 regularization. default = 0.01
                (7) list 'n_filters': number of filters for each convolution layer (4 total)
                (8) list 'k_dims': dimension of kernel at each layer (k_dim[n] x k_dim[n] square) with 4 total
                (9) string 'activation': activation to use at each convolutional layer. default = 'relu'
        '''
        self.n_epoch = n_epoch
        self.n_chan = n_chan
        self.batch_size = batch_size
        self.architecture = architecture
        self.augment_data = augment_data
        self.gen_multiplier = gen_multiplier
        self.multi_gpu = multi_gpu
        self.num_gpu = num_gpu
        self.loaded_model = loaded_model
        self.custom_metric = custom_metric
        self.w_reg = w_reg
        self.n_filters = n_filters
        self.k_dims = k_dims
        self.activation = activation
        if not self.loaded_model:
            if self.architecture == 'singlev1':
                self.model_comp = models_single.comp_singlev1(multi_gpu, num_gpu)
            elif self.architecture == 'singlev2':
                self.model_comp = models_single.comp_singlev2(multi_gpu, num_gpu)
            elif self.architecture == 'singlev3':
                self.model_comp = models_single.comp_singlev3(multi_gpu, num_gpu)
            elif self.architecture == 'vgg19':
                self.model_comp = models_single.comp_vgg19(multi_gpu, num_gpu)
            elif self.architecture == 'small_alex':
                self.model_comp = models_single.comp_small_alex(multi_gpu, num_gpu)
            elif self.architecture == 'segnet':
                self.model_comp = models_single.comp_segnet(multi_gpu, num_gpu)
            elif self.architecture == 'dual':
                self.model_comp = models_double.comp_dual(multi_gpu, num_gpu)
            elif self.architecture == 'two_path':
                self.model_comp = models_double.comp_two_path(multi_gpu, num_gpu)
            elif self.architecture == 'resnet50':
                self.model_comp = resnet.comp_resnet50(multi_gpu, num_gpu)
            elif self.architecture == 'inceptionv3':
                self.model_comp = inceptionv3.comp_inceptionv3(multi_gpu, num_gpu)
            elif self.architecture == 'inception_resnetv1':
                self.model_comp = inception_resnetv1.comp_inception_resnetv1(multi_gpu, num_gpu)
        else:
            model = str(raw_input('Which model should I load?'))
            if(custom_metric):
                self.model_comp = load_model(model, custom_objects={'dice_coef': dice_coef, 'dice_coef_neg': dice_coef_neg, 'jaccard_similarity': jaccard_similarity, 'jaccard_similarity_int': jaccard_similarity_int})
            else:
                self.model_comp = load_model(model)

    def fit_model(self, X_train, y_train, X3_train, X_val, y_val, X3_val, save=True):
        '''
        INPUT:  (1) numpy array 'X_train': list of patches to train on in form (n_sample, n_channel, h, w)
                (2) numpy vector 'y_train': list of labels corresponding to X_train patches in form (n_sample,)
                (3) numpy array 'X3_train': center 3x3 patch in corresponding X_train patch. if None, uses single-path
                    architecture
        OUTPUT: (1) fits specified model
        '''
        Y_train = np_utils.to_categorical(y_train, 5)
        Y_val = np_utils.to_categorical(y_val, 5)
        shuffle_train = zip(X_train, Y_train)
        shuffle_val = zip(X_val, Y_val)
        np.random.shuffle(shuffle_train)
        np.random.shuffle(shuffle_val)

        X_train = np.array([shuffle_train[i][0] for i in xrange(len(shuffle_train))])
        Y_train = np.array([shuffle_train[i][1] for i in xrange(len(shuffle_train))])
        X_val = np.array([shuffle_val[i][0] for i in xrange(len(shuffle_val))])
        Y_val = np.array([shuffle_val[i][1] for i in xrange(len(shuffle_val))])

        es = EarlyStopping(monitor='val_loss', patience=1, verbose=1, mode='auto')

        # Save model after each epoch to check/bm_epoch#-val_loss
        checkpointer = ModelCheckpoint(filepath="./check/bm_{epoch:02d}-{val_loss:.2f}.hdf5", verbose=1)

        if self.augment_data:
            datagen = ImageDataGenerator(
                rotation_range=90,
                #horizontal_flip=True,
                vertical_flip=True,
                fill_mode='nearest'
            )

            val_datagen = ImageDataGenerator()
            gen_multiplier = self.gen_multiplier
            train_samples_to_gen = len(X_train) * gen_multiplier
            val_samples_to_gen = len(X_val)

            datagen.fit(X_train)
            print 'Generating and using augmented data (x{})...'.format(gen_multiplier)

            if self.architecture == 'singlev1' or self.architecture == 'singlev2' or self.architecture == 'singlev3' or self.architecture == 'vgg19' or self.architecture == 'small_alex' or self.architecture == 'segnet' or self.architecture == 'resnet50' or self.architecture == 'inceptionv3' or self.architecture == 'inception_resnetv1':
                self.model_comp.fit_generator(datagen.flow(X_train, Y_train, batch_size=self.batch_size),  samples_per_epoch=train_samples_to_gen, nb_epoch=self.n_epoch,
                validation_data=val_datagen.flow(X_val, Y_val, batch_size=self.batch_size), nb_val_samples=val_samples_to_gen,
                verbose=1, callbacks=[checkpointer, es])
            elif self.architecture == 'dual' or self.architecture == 'two_path':
                self.model_comp.fit_generator(datagen.flow([X3_train, X_train], Y_train, batch_size=self.batch_size),  samples_per_epoch=train_samples_to_gen, nb_epoch=self.n_epoch,
                validation_data=val_datagen.flow(X_val, Y_val, batch_size=self.batch_size), nb_val_samples=val_samples_to_gen,
                verbose=1, callbacks=[checkpointer, es])
        else:
            if self.architecture == 'singlev1' or self.architecture == 'singlev2' or self.architecture == 'singlev3' or self.architecture == 'vgg19' or self.architecture == 'small_alex' or self.architecture == 'segnet' or self.architecture == 'resnet50' or self.architecture == 'inceptionv3' or self.architecture == 'inception_resnetv1':
                self.model_comp.fit(X_train, Y_train, nb_epoch=self.n_epoch, batch_size=self.batch_size, validation_split=0.1,
                                    verbose=1, callbacks=[checkpointer, es])
            elif self.architecture == 'dual' or self.architecture == 'two_path':
                self.model_comp.fit([X3_train, X_train], Y_train, batch_size=self.batch_size, nb_epoch=self.n_epoch,
                                    validation_split=0.1, verbose=1, callbacks=[checkpointer, es]) 
        
    def save_model(self, model_name):
        '''
        INPUT:  string 'model_name': name to save model and weights under, including filepath but not extension
        Saves current model as json and weights as h5df file
        '''
        self.model_comp.save('models/{}.h5'.format(model_name))

    def class_report(self, X_test, y_test):
        '''
        returns sklearn test report (precision, recall, f1-score)
        INPUT:  (1) list 'X_test': test data of 4x33x33 patches
                (2) list 'y_test': labels for X_test
        OUTPUT: (1) confusion matrix of precision, recall, and f1 score
        '''
        y_pred = self.model_comp.predict_classes(X_test)
        print classification_report(y_pred, y_test)

    def predict_image(self, test_img, show=False):
        '''
        predicts classes of input image
        INPUT:  (1) str 'test_img': filepath to image to predict on
                (2) bool 'show': True to show the results of prediction, False to return prediction
        OUTPUT: (1) if show == False: array of predicted pixel classes for the center 208x208 pixels
                (2) if show == True: displays segmentation results
        '''
        imgs = io.imread(test_img).astype('float').reshape(5,240,240)
        p5list, plist, patches  = [], [], []

        def center_n(n, patches):
            '''
            Takes list of patches and returns center nxn for each patch. Use as input for cascaded architectures.
            INPUT:  (1) int 'n': size of center patch to take (square)
                    (2) list 'patches': list of patches to take subpatch of
            OUTPUT: list of center nxn patches
            '''
            sub_patches = []
            height, width = 33, 33
            for mode in patches:
                subs = np.array([patch[(height/2) - (n/2):(height/2) + ((n+1)/2), (width/2) - (n/2):(width/2) + 
                                ((n+1)/2)] for patch in mode])
                sub_patches.append(subs)
            return np.array(sub_patches)

        # create patches from an entire slice
        for img in imgs[:-1]:
            if np.max(img) != 0:
                img /= np.max(img)

            p = extract_patches_2d(img, (33,33))
            plist.append(p)
                
        if (self.architecture == 'singlev1') or (self.architecture == 'singlev2') or (self.architecture == 'singlev3') or (self.architecture == 'vgg19') or (self.architecture == 'small_alex') or (self.architecture == 'segnet') or (self.architecture == 'resnet50') or (self.architecture == 'inceptionv3') or (self.architecture == 'inception_resnetv1'):
            patches = np.array(zip(np.array(plist[0]), np.array(plist[1]), np.array(plist[2]), np.array(plist[3])))
        elif self.architecture == 'dual':
            patches = np.array(zip(np.array(plist[0]), np.array(plist[1]), np.array(plist[2]), np.array(plist[3])))
            patches5 = center_n(5, patches)
            patches = [patches5, patches]
        elif self.architecture == 'two_path':
            patches = np.array(zip(np.array(plist[0]), np.array(plist[1]), np.array(plist[2]), np.array(plist[3])))
            patches = [patches, patches]

        # predict classes of each pixel based on model
        if(self.architecture == 'resnet50') or (self.architecture == 'inceptionv3') or (self.architecture == 'inception_resnetv1') :
            y_proba = self.model_comp.predict(patches)
            full_pred = probas_to_classes(y_proba)
        else:
            full_pred = self.model_comp.predict_classes(patches)
        
        fp1 = full_pred.reshape(208,208)
        if show:
            io.imshow(fp1)
            plt.show
        else: 
            return fp1

    def overlay_segmentation(self, test_img, mask_label):
        '''
        INPUT:  (1)
        '''
        green_rgb = [0.05,0.47,0.07] 
        yellow_rgb = [0.94,0.80,0.24] 
        red_rgb = [0.81,0.25,0.25] 
        blue_rgb = [0,0.95,1.0] 

        ones = np.argwhere(mask_label == 1)
        twos = np.argwhere(mask_label == 2)
        threes = np.argwhere(mask_label == 3)
        fours = np.argwhere(mask_label == 4)

        # change colors of predicted segmented classes
        for i in xrange(len(ones)):
            test_img[ones[i][0]][ones[i][1]] = green_rgb # necrotic core
        for i in xrange(len(twos)):
            test_img[twos[i][0]][twos[i][1]] = yellow_rgb # edema
        for i in xrange(len(threes)):
            test_img[threes[i][0]][threes[i][1]] = red_rgb # non-advancing/enhancing solid core
        for i in xrange(len(fours)):
            test_img[fours[i][0]][fours[i][1]] = blue_rgb # advancing/enhancing tumor 

        return test_img

    def segment_image(self, test_img, label_data, test_label):
        '''
        Creates an image of original brain with segmentation overlay
        INPUT:  (1) str 'test_img': filepath to test image for segmentation, including file extension
                (3) bool 'show': if True, shows output image. default = False
        OUTPUT: (1) if show == True, shows image of segmentation results
                (2) if show == False, returns segmented image
        '''
        base_img = io.imread(test_img)
        base_img = base_img.reshape(5,240,240)[-2]
        base_img = img_as_float(base_img)

        base_img = adjust_gamma(color.gray2rgb(base_img), 0.65) # adjust gamma of image
        slices_for_gt = base_img.copy()
        slices_for_pred = base_img.copy()

        # Generate ground truth segmentation
        gt_label = io.imread(test_label).astype(int)
        gt_seg_slice = self.overlay_segmentation(slices_for_gt, gt_label)

        # Generate predicted segmentation
        segmentation = self.predict_image(test_img, show=False)
        pred_label = np.pad(segmentation, (16,16), mode='edge')
        pred_seg_slice = self.overlay_segmentation(slices_for_pred, pred_label)

        os.getcwd()
        testDirPath = 'seg_{}_{}'.format(self.architecture, label_data)
        os.makedirs('seg_img_results/' + testDirPath)
        print 'Saving ground truth segmented slice...'
        io.imsave('seg_img_results/' + testDirPath + '/gt_{}({}).png'.format(self.architecture, label_data), gt_seg_slice)
        print 'Saving predicted segmented slice...'
        io.imsave('seg_img_results/' + testDirPath + '/pred_{}({}).png'.format(self.architecture, label_data), pred_seg_slice)

    def cal_metrics(self, test_img, label):
        '''
        Calculate dice coefficient for total slice, tumor-associated slice, advancing tumor and core tumor
        INPUT:  (1) str 'test_img': filepath to slice to predict on
                (2) str 'label': filepath to ground truth label for test_img
        OUTPUT: Summary of dice scores for the following classes:
                    - all classes
                    - all classes excluding background (ground truth and segmentation)
                    - all classes excluding background (only ground truth)
                    - advancing tumor
                    - core tumor (1, 3, 4)
        '''
        gt_label = io.imread(test_label).astype(int)

        necrosis = np.argwhere(gt_label == 1)
        edema = np.argwhere(gt_label == 2)
        non_adv = np.argwhere(gt_label == 3)
        adv = np.argwhere(gt_label == 4)

        segmentation = self.predict_image(test_img)
        pred_label = np.pad(segmentation, (16,16), mode='edge')

        # Calculate whole tumor dice
        wt_dice = metric.binary.dc(gt_label, pred_label)

        # Calculate enhancing tumor dice
        gt_adv, seg_adv = [], [] 
        for i in adv:
            gt_adv.append(gt_label[i[0]][i[1]])
            seg_adv.append(pred_label[i[0]][i[1]])
        gt_label_adv = np.array(gt_adv)
        pred_label_adv = np.array(seg_adv)

        if len(adv) > 0:
            et_dice = float(len(np.argwhere(gt_label_adv == pred_label_adv))) / len(adv)
        else:
            et_dice = 0.0

        # Calculate tumor core dice
        live_tumor = np.append(adv, non_adv, axis=0)
        core = np.append(live_tumor, necrosis, axis=0)
        gt_core, seg_core = [], []
        for i in core:
            gt_core.append(gt_label[i[0]][i[1]])
            seg_core.append(pred_label[i[0]][i[1]])
        gt_core, seg_core = np.array(gt_core), np.array(seg_core)

        if len(core) > 0:
            tc_dice = len(np.argwhere(gt_core == seg_core)) / float(len(core))
        else:
            tc_dice = 0.0

        # Calculate hausdorff dist, sensitivity, specificity
        hausdorff_dist = metric.binary.hd(gt_label, pred_label, connectivity=1)
        sensitivity = metric.binary.recall(gt_label, pred_label)

        def specificity(true, pred):
            '''
            INPUT   (1)
            '''
            pred = np.atleast_1d(pred.astype(np.bool))
            true = np.atleast_1d(true.astype(np.bool))

            TN = np.count_nonzero(~pred & ~true)
            FP = np.count_nonzero(pred & ~true)

            return TN / float(TN + FP)

        specificity = specificity(gt_label, pred_label)

        print '\nDice (Whole Tumor)_____________| {0:.2f}'.format(wt_dice)
        print 'Dice (Enhancing Tumor)_________| {0:.2f}'.format(et_dice)
        print 'Dice (Tumor Core)______________| {0:.2f}'.format(tc_dice)
        print 'Hausdorff Distance_____________| {0:.2f}'.format(hausdorff_dist)
        print 'Sensitivity____________________| {0:.2f}'.format(sensitivity)
        print 'Specificity____________________| {0:.2f}\n'.format(specificity)

    def get_hausdorff_dist():
        print 'TODO'

if __name__ == '__main__':
    def make_dirs():
        os.getcwd()
        os.makedirs('check')
        os.makedirs('models')
        os.makedirs('seg_img_results')
        os.makedirs('test_data')

        source = 'N4_PNG'
        dest = 'test_data'
        files = os.listdir(source)
        for f in files:
            if f.startswith('0'):
                shutil.move(source + '/' + f, dest)
            elif f.startswith('1'):
                shutil.move(source + '/' + f, dest)
            elif f.startswith('2'):
                shutil.move(source + '/' + f, dest)

    def test_segmentation(model):
        print 'Testing segmentation on %s slice...' % (label_data)
        model.segment_image(test_img=test_data, label_data=label_data, test_label=test_label)

    def cal_metrics(model):
        print 'Calculating metrics on %s slice...' % (label_data)
        model.cal_metrics(test_img=test_data, label=test_label)

    def load_data(train_patches_path, val_patches_path):
        # Load saved patches
        print 'Loading saved training patches...'
        X_train_h5_file = h5py.File(train_patches_path + '/saved_X.h5', 'r')
        X_train = X_train_h5_file['X'][:]
        X_train_h5_file.close()
        y_train_h5_file = h5py.File(train_patches_path + '/saved_y.h5', 'r')
        y_train = y_train_h5_file['y'][:]
        y_train_h5_file.close()

        print 'Loading saved validation patches...'
        X_val_h5_file = h5py.File(val_patches_path + '/saved_X.h5', 'r')
        X_val = X_val_h5_file['X'][:]
        X_val_h5_file.close()
        y_val_h5_file = h5py.File(val_patches_path + '/saved_y.h5', 'r')
        y_val = y_val_h5_file['y'][:]
        y_val_h5_file.close()

        return X_train, y_train, X_val, y_val

    def test_model(architecture, loaded_model, custom_metric, two_path, augment_data, gen_multiplier, multi_gpu, num_gpu,
        n_epoch, action):
        print 'Using %s model...' % (architecture)
        model = NN_Models(architecture=architecture, loaded_model=loaded_model, custom_metric=custom_metric, augment_data=augment_data, gen_multiplier=gen_multiplier, multi_gpu=multi_gpu, num_gpu=num_gpu, n_epoch=n_epoch)
        
        if action == 'train':
            X_train, y_train, X_val, y_val = load_data(train_patches_path, val_patches_path)
            if not two_path:
                model.fit_model(X_train=X_train, y_train=y_train, X3_train=None, X_val=X_val, y_val=y_val, X3_val=None)
                model.save_model('model001_{}'.format(architecture))
                #model.class_report(X, y)
            else:
                model.fit_model(X_train=X_train, y_train=y_train, X3_train=X3_train, X_val=X_val, y_val=y_val, X3_val=X3_val)
                model.save_model('model001_{}'.format(architecture))
                #model.class_report([X, X], y)
        elif action == 'segment':
            test_segmentation(model)
        elif action == 'metrics':
            cal_metrics(model)
        
'''
Start of main 
'''
#make_dirs() # comment out once directory is made 

label_0_93, test_data_0_93, test_label_0_93 = '0_93', 'test_data/0_93.png', 'Labels/0_93L.png'
label_1_59, test_data_1_59, test_label_1_59 = '1_59', 'test_data/1_59.png', 'Labels/1_59L.png'
label_2_106, test_data_2_106, test_label_2_106 = '2_106', 'test_data/2_106.png', 'Labels/2_106L.png'
label_3_60, test_data_3_60, test_label_3_60 = '3_60', 'test_data/3_60.png', 'Labels/3_60L.png'
label_4_92, test_data_4_92, test_label_4_92 = '4_92', 'test_data/4_92.png', 'Labels/4_92L.png'
label_5_88, test_data_5_88, test_label_5_88 = '5_88', 'test_data/5_88.png', 'Labels/5_88L.png'

# Real
train_patches_path_10k, val_patches_path_1k = 'patches/Original10k_Train', 'patches/Original1k_Valid'
train_patches_path_20k, val_patches_path_2k = 'patches/Original20k_Train', 'patches/Original2k_Valid'
train_patches_path_75k, val_patches_path_7_5k = 'patches/Original75k_Train', 'patches/Original7.5k_Valid'
train_patches_path_400k, val_patches_path_40k = 'patches/Original400k_Train', 'patches/Original40k_Valid'
train_patches_path_800k, val_patches_path_80k = 'patches/Original800k_Train', 'patches/Original80k_Valid'
# Generated
train_patches_path_gen10k, val_patches_patch_1k = 'patches/Gen10k_Train', 'patches/Original1k_Valid'
# Augmented Data
train_patches_path_aug75k, val_patches_path_7_5k = 'patches/Aug75k_Train', 'patches/Original7.5k_Valid'
train_patches_path_aug400k, val_patches_path_40k = 'patches/Aug400k_Train', 'patches/Original40k_Valid'

# File paths
label_data, test_data, test_label = label_0_93, test_data_0_93, test_label_0_93
train_patches_path, val_patches_path = train_patches_path_aug400k, val_patches_path_40k

'''
String 'architecture': options(singlev1, singlev2, singlev3, vgg19, small_alex, segnet, dual, two_path, resnet50,
    inceptionv3, inception_resnetv1)
String 'action': options(train, segment, metrics)
'''
test_model(architecture='resnet50', 
           loaded_model=True, 
           custom_metric=True,
           two_path=False,
           augment_data=False, 
           gen_multiplier=12,
           multi_gpu=False,
           num_gpu=2,
           n_epoch=50,
           action='segment')




