import numpy as np
import random, os, h5py, matplotlib
import matplotlib.pyplot as plt
from glob import glob
from skimage import io
from skimage.filters.rank import entropy
from skimage.morphology import disk
from skimage.transform import rotate
from sklearn.feature_extraction.image import extract_patches_2d

np.random.seed(1)

class PatchLib(object):

    def __init__(self, patch_size, train_data, num_samples, augmentation):
        '''
        class for creating patches and subpatches from training data to use as input for nn models.
        INPUT:  (1) tuple 'patch_size': size (in voxels) of patches to extract. Use (33,33) for sequential model
                (2) list 'train_data': list of filepaths to all training data saved as png. images should have shape 
                    (5*240,240)
                (3) int 'num_samples': the number of patches to collect from training data
        '''
        self.patch_size = patch_size
        self.num_samples = num_samples
        self.train_data = train_data
        self.augmentation = augmentation
        self.h = self.patch_size[0]
        self.w = self.patch_size[1]

    def find_patches(self, class_num, num_patches):
        '''
        Helper function for sampling slices with evenly distributed classes
        INPUT:  (1) list 'training_images': all training images to select from
                (2) int 'class_num': class to sample from choice of {0, 1, 2, 3, 4}
                (3) tuple 'patch_size': dimensions of patches to be generated
        OUTPUT: (1) num_samples patches from class 'class_num' randomly selected
        '''
        h, w = self.patch_size[0], self.patch_size[1]
        patches, labels = [], np.full(num_patches, class_num, 'float')
        print 'Finding patches of class {}...'.format(class_num)

        count = 0
        while count < num_patches:
            im_path = random.choice(self.train_data)
            filename = os.path.basename(im_path)
            label = io.imread('Labels/' + filename[:-4] + 'L.png')

            # resample if class_num not in selected slice
            # while len(np.argwhere(label == class_num)) < 10:
            #   im_path = random.choice(self.train_data)
            #   filename = os.path.basename(im_path)
            #   label = io.imread('Labels/' + filename[:-4] + 'L.png')
            if len(np.argwhere(label == class_num)) < 10:
                continue

            # select centerpixel (p) and patch (p_num)
            img = io.imread(im_path).reshape(5,240,240)[:-1].astype('float')
            p = random.choice(np.argwhere(label == class_num))
            p_num = (p[0]-(h/2), p[0]+((h+1)/2), p[1]-(w/2), p[1]+((w+1)/2))

            if self.augmentation == 'rotate':
                img_to_rotate = img.copy()
                rand_rotation = random.randrange(-25, 25)
                img_rotated = rotate(img_to_rotate, rand_rotation, mode='symmetric')
                patch_rotated = np.array([i[p_num[0]:p_num[1], p_num[2]:p_num[3]] for i in img_rotated])

                if patch_rotated.shape != (4, h, w) or len(np.argwhere(patch_rotated == 0)) > (h * w):
                    continue

                patches.append(patch_rotated)
            else:
                patch = np.array([i[p_num[0]:p_num[1], p_num[2]:p_num[3]] for i in img])

                # resample if patch is empty or too close to edge
                # while patch.shape != (4, h, w) or len(np.unique(patch)) == 1:
                #   p = random.choice(np.argwhere(label == class_num))
                #   p_num = (p[0]-(h/2), p[0]+((h+1)/2), p[1]-(w/2), p[1]+((w+1)/2))
                #   patch = np.array([i[p_num[0]:p_num[1], p_num[2]:p_num[3]] for i in img])
                if patch.shape != (4, h, w) or len(np.argwhere(patch == 0)) > (h * w):
                    continue

                patches.append(patch)
            
            count += 1
            if(count % (num_patches / 10) == 0):
                print 'progress count = {} patches found...'.format(count)
        return np.array(patches), labels

    def center_n(self, n, patches):
        '''
        Takes list of patches and returns center nxn for each patch. Use as input for cascaded architectures.
        INPUT:  (1) int 'n': size of center patch to take (square)
                (2) list 'patches': list of patches to take subpatch of
        OUTPUT: list of center nxn patches
        '''
        sub_patches = []
        for mode in patches:
            subs = np.array([patch[(self.h/2) - (n/2):(self.h/2) + ((n+1)/2), (self.w/2) - (n/2):(self.w/2) + ((n+1)/2)] for             patch in mode])
            sub_patches.append(subs)
        return np.array(sub_patches)

    def slice_to_patches(self, filename):
        '''
        Converts an image to a list of patches with stride length of 1. Use as input for image prediction.
        INPUT:  str 'filename': path to image to be converted to patches
        OUTPUT: list of patched versions of input image.
        '''
        slices = io.imread(filename).astype('float').reshape(5,240,240)[:-1]
        plist = []
        for slice in slices:
            if np.max(img) != 0:
                img /= np.max(img)
            p = extract_patches_2d(img, (h,w))
            plist.append(p)
        return np.array(zip(np.array(plist[0]), np.array(plist[1]), np.array(plist[2]), np.array(plist[3])))

    def patches_by_entropy(self, num_patches):
        '''
        Finds high-entropy patches based on label, allows net to learn borders more effectively
        INPUT:  int 'num_patches': defaults to num_samples, enter in quantity in conjunction with randomly sampled patches
        OUTPUT: list of patches (num_patches, 4, h, w) selected by highest entropy
        '''
        patches, labels = [], []
        count = 0
        while count < num_patches:
            im_path = random.choice(training_images)
            filename = os.path.basename(im_path)
            label = io.imread('Labels/' + filename[:-4] + 'L.png')

            # pick again if slice is only background
            if len(np.unique(label)) == 1:
                continue

            img = io.imread(im_path).reshape(5,240,240)[:-1].astype('float')
            l_ent = entropy(label, disk(self.h))
            top_ent = np.percentile(l_ent, 90)

            # restart if 80th entropy percentile = 0
            if top_ent == 0:
                continue

            highest = np.argwhere(l_ent >= top_ent)
            p_s = random.sample(highest, 3)
            for p in p_s:
                p_num = (p[0]-(h/2), p[0]+((h+1)/2), p[1]-(w/2), p[1]+((w+1)/2))
                patch = np.array([i[p_num[0]:p_num[1], p_num[2]:p_num[3]] for i in img])
                # exclude any patches that are too small
                if np.shape(patch) != (4, 65, 65):
                    continue
                patches.append(patch)
                labels.append(label[p[0], p[1]])
            count += 1
            return np.array(patches[:num_samples]), np.array(labels[:num_samples])

    def make_training_patches(self, entropy=False, balanced_classes=True, classes=[0,1,2,3,4]):
        '''
        Creates X and y for training CNN
        INPUT   (1) bool 'entropy': if True, half of the patches are chosen based on highest entropy area. defaults to 
                    False.
                (2) bool 'balanced_classes': if True, produces an equal number of each class from the randomly chosen 
                    samples
                (3) list 'classes': list of classes to sample from. Only change defaults if entropy is False and 
                    balanced_classes is True
        OUTPUT  (1) X: patches(num_samples, 4_chan, h, w)
                (2) y: labels (num_samples,)
        '''
        if self.augmentation == 'rotate':
            print 'Generating patches using rotation...'
        else:
            print 'Generating normal patches...'

        if balanced_classes:
            per_class = self.num_samples / len(classes)
            patches, labels = [], []
            for i in xrange(len(classes)):
                p, l = self.find_patches(classes[i], per_class)
                # set 0 <= pixel intensity <= 1
                for img_num in xrange(len(p)):
                    for slice in xrange(len(p[img_num])):
                        if np.max(p[img_num][slice]) != 0:
                            p[img_num][slice] /= np.max(p[img_num][slice])
                patches.append(p)
                labels.append(l)
            return np.array(patches).reshape(self.num_samples, 4, self.h, self.w), np.array(labels).reshape(self.num_samples)
        else:
            print "Use balanced classes, random won't work."

def combine_patches():
    '''
    INPUT: (1)
    '''
    class_0_patch_path = 'patches/Original1.6m_Train/class_0/'
    class_1_patch_path = 'patches/Original1.6m_Train/class_1/'
    class_2_patch_path = 'patches/Original1.6m_Train/class_2/'
    class_3_patch_path = 'patches/Original1.6m_Train/class_3/'
    class_4_patch_path = 'patches/Original1.6m_Train/class_4/'

    print 'Loading class 0 saved training patches...'
    X_class_0_h5 = h5py.File(class_0_patch_path + '/saved_X.h5', 'r')
    X_class_0_train = X_class_0_h5['X'][:]
    X_class_0_h5.close()
    y_class_0_h5 = h5py.File(class_0_patch_path + '/saved_y.h5', 'r')
    y_class_0_train = y_class_0_h5['y'][:]
    y_class_0_h5.close()

    print 'Loading class 1 saved training patches...'
    X_class_1_h5 = h5py.File(class_1_patch_path + '/saved_X.h5', 'r')
    X_class_1_train = X_class_1_h5['X'][:]
    X_class_1_h5.close()
    y_class_1_h5 = h5py.File(class_1_patch_path + '/saved_y.h5', 'r')
    y_class_1_train = y_class_1_h5['y'][:]
    y_class_1_h5.close()

    print 'Loading class 2 saved training patches...'
    X_class_2_h5 = h5py.File(class_2_patch_path + '/saved_X.h5', 'r')
    X_class_2_train = X_class_2_h5['X'][:]
    X_class_2_h5.close()
    y_class_2_h5 = h5py.File(class_2_patch_path + '/saved_y.h5', 'r')
    y_class_2_train = y_class_2_h5['y'][:]
    y_class_2_h5.close()

    print 'Loading class 3 saved training patches...'
    X_class_3_h5 = h5py.File(class_3_patch_path + '/saved_X.h5', 'r')
    X_class_3_train = X_class_3_h5['X'][:]
    X_class_3_h5.close()
    y_class_3_h5 = h5py.File(class_3_patch_path + '/saved_y.h5', 'r')
    y_class_3_train = y_class_3_h5['y'][:]
    y_class_3_h5.close()

    print 'Loading class 4 saved training patches...'
    X_class_4_h5 = h5py.File(class_4_patch_path + '/saved_X.h5', 'r')
    X_class_4_train = X_class_4_h5['X'][:]
    X_class_4_h5.close()
    y_class_4_h5 = h5py.File(class_4_patch_path + '/saved_y.h5', 'r')
    y_class_4_train = y_class_4_h5['y'][:]
    y_class_4_h5.close()

    X_train = np.concatenate([X_class_0_train, X_class_1_train, X_class_2_train, X_class_3_train, X_class_4_train])
    y_train = np.concatenate([y_class_0_train, y_class_1_train, y_class_2_train, y_class_3_train, y_class_4_train])
    return X_train, y_train

if __name__ == '__main__':
    # Tissue classifications
    healthy_class = 0
    necrotic_core_class = 1
    edema_class = 2
    nonadv_core_class = 3
    adv_core_class = 4

    tissue_name, all_tissues = "full", [0, 1, 2, 3, 4]
    tissue_type = healthy_class

    train_imgs = glob('N4_PNG/**')
    #patch_path = 'patches/Original100k_class_{}/'.format(adv_core_class)
    patch_path = 'patches/Original75k_Train/'
    num_gen = 75000

    '''
    string 'augmentation': options (rotation, none)
    '''
    print 'Generating {} patches from normal images...'.format(num_gen)
    patch1 = PatchLib(patch_size=(40,40), 
                      train_data=train_imgs, 
                      num_samples=num_gen, 
                      augmentation='none')
    X1, y1 = patch1.make_training_patches(classes=all_tissues)

    X = X1
    y = y1

    # Patches for single models
    print 'Saving X and y patches...'
    X_h5_file = h5py.File(patch_path + 'saved_X.h5', 'w')
    X_h5_file.create_dataset('X', data=X)
    X_h5_file.close()

    y_h5_file = h5py.File(patch_path + 'saved_y.h5', 'w')
    y_h5_file.create_dataset('y', data=y)
    y_h5_file.close()

    # # Patches for dual models
    # print 'Saving X3 patches...'
    # X3 = patch1.center_n(3, X)
    # X3_h5_file = h5py.File(patch_path + 'saved_X3.h5', 'w')
    # X3_h5_file.create_dataset('X3', data=X3)
    # X3_h5_file.close()

    # patch_path = 'patches/Original1.6m_Train/'
    # X, y = combine_patches();
    # print 'Saving X and y patches...'
    # X_h5_file = h5py.File(patch_path + 'saved_X.h5', 'w')
    # X_h5_file.create_dataset('X', data=X)
    # X_h5_file.close()

    # y_h5_file = h5py.File(patch_path + 'saved_y.h5', 'w')
    # y_h5_file.create_dataset('y', data=y)
    # y_h5_file.close()
    




    
    
