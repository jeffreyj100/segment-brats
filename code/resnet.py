import numpy as np
import warnings
from keras.layers import merge, Input
from keras.layers import Dense, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers import BatchNormalization
from keras.models import Model
from keras.optimizers import SGD, Nadam
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.utils.data_utils import get_file

from metrics import dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int
from multi_gpu import make_parallel

def identity_block(input_tensor, kernel_size, filters, stage, block):
    '''
    Block that has no conv layer at shortcut
    INPUT:  (1) input_tensor: input tensor
            (2) kernel_size: kernel size of middle conv layer at main path, default = 3
            (3) filters: nb_filters of 3 conv layer at main path, list of integers
            (4) stage: current stage label used for generating layer names, integer
            (5) block: current block label, used for generating layer names, ex. 'a', 'b'
    OUTPUT: (1) x: output layer
    '''
    nb_filter1, nb_filter2, nb_filter3 = filters
    bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Convolution2D(nb_filter1, 1, 1, name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Convolution2D(nb_filter2, kernel_size, kernel_size, border_mode='same', name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Convolution2D(nb_filter3, 1, 1, name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    x = merge([x, input_tensor], mode='sum')
    x = Activation('relu')(x)
    return x

def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
    '''
    Block that has a conv layer at shortcut
    INPUT:  (1) input_tensor: input tensor
            (2) kernel_size: kernel size of middle conv layer at main path, default = 3
            (3) filters: nb_filters of 3 conv layer at main path, list of integers
            (4) stage: current stage label used for generating layer names, integer
            (5) block: current block label, used for generating layer names, ex. 'a', 'b'
    OUTPUT: (1) x: output layer
    '''
    nb_filter1, nb_filter2, nb_filter3 = filters
    bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Convolution2D(nb_filter1, 1, 1, subsample=strides, name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Convolution2D(nb_filter2, kernel_size, kernel_size, border_mode='same', name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Convolution2D(nb_filter3, 1, 1, name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    shortcut = Convolution2D(nb_filter3, 1, 1, subsample=strides, name=conv_name_base + '1')(input_tensor)
    shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)

    x = merge([x, shortcut], mode='sum')
    x = Activation('relu')(x)
    return x

def comp_resnet50(multi_gpu, num_gpu):
    '''
    compiles 50 layer resnet
    '''
    print 'Compiling resnet50 ...'
    input_shape = (4, 33, 33)
    img_input = Input(shape=input_shape)
    bn_axis = 1

    x = ZeroPadding2D((3, 3))(img_input)
    x = Convolution2D(16, 4, 4, subsample=(2, 2), name='conv1')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2))(x)

    x = conv_block(x, 3, [16, 16, 64], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [16, 16, 64], stage=2, block='b')
    x = identity_block(x, 3, [16, 16, 64], stage=2, block='c')

    x = conv_block(x, 3, [32, 32, 128], stage=3, block='a')
    x = identity_block(x, 3, [32, 32, 128], stage=3, block='b')
    x = identity_block(x, 3, [32, 32, 128], stage=3, block='c')
    x = identity_block(x, 3, [32, 32, 128], stage=3, block='d')

    x = conv_block(x, 3, [64, 64, 256], stage=4, block='a')
    x = identity_block(x, 3, [64, 64, 256], stage=4, block='b')
    x = identity_block(x, 3, [64, 64, 256], stage=4, block='c')
    x = identity_block(x, 3, [64, 64, 256], stage=4, block='d')
    x = identity_block(x, 3, [64, 64, 256], stage=4, block='e')
    x = identity_block(x, 3, [64, 64, 256], stage=4, block='f')

    x = conv_block(x, 3, [128, 128, 512], stage=5, block='a')
    x = identity_block(x, 3, [128, 128, 512], stage=5, block='b')
    x = identity_block(x, 3, [128, 128, 512], stage=5, block='c')

    x = AveragePooling2D((1, 1), name='avg_pool')(x)

    x = Flatten()(x)
    x = Dense(256)(x)
    x = Dense(5, activation='softmax')(x)

    resnet50 = Model(img_input, x)

    if multi_gpu:
        resnet50 = make_parallel(resnet50, num_gpu)

    nadam = Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004)
    resnet50.compile(optimizer='nadam', loss='categorical_crossentropy', metrics=[dice_coef, dice_coef_neg, jaccard_similarity, jaccard_similarity_int])
    print 'Complete.'
    return resnet50